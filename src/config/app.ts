export const app = {
  name: process.env.APP_NAME,

  urls: {
    api: process.env.API_URL ?? 'http://localhost:4000/dev',
    upload: process.env.UPLOAD_URL ?? 'http://localhost:4000/dev',
  },

  cors: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Allow-Methods': 'OPTIONS,GET,POST,PUT,DELETE',
    'Access-Control-Allow-Credentials': true,
  },
};
