export const logger = {
  level: process.env.LOG_LEVEL ?? 'info',
};
