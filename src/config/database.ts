export const database = {
  mongo: {
    host: process.env.MONGO_HOST ?? 'localhost',
    port: process.env.MONGO_PORT ?? '27017',
    user: process.env.MONGO_USER ?? 'user',
    password: process.env.MONGO_PASSWORD ?? 'password',
    database: process.env.MONGO_DATABASE ?? 'local',
    uri: process.env.MONGO_URI,
  },
};
