import * as _ from 'lodash';
import { validate, validateAll } from 'indicative/validator';
import { ValidatorConfig as IndicativeValidatorConfig } from 'indicative/src/Contracts';

import { lambda } from '@helpers';

export interface ValidatorConfig extends IndicativeValidatorConfig {
  isValidateAll?: boolean;
}

export interface Error {
  message: string;
  validation: string;
  field: string;
}

const DEFAULT_CONFIG: Partial<ValidatorConfig> = {
  isValidateAll: true,
};

function common(
  section: string,
  schema: any,
  messages?: any,
  options?: Partial<ValidatorConfig>
) {
  return async (request: lambda.Request, callback, next) => {
    options = Object.assign({}, DEFAULT_CONFIG, options);

    const validateMethod = options.isValidateAll ? validateAll : validate;

    delete options.isValidateAll;

    try {
      request[section] = await validateMethod(
        request[section] ?? {},
        schema,
        messages,
        options
      );
    } catch (errors) {
      request.locals = {
        ...request.locals,
        validationErrors: {
          ...request.locals.validationErrors,
          [section]: errors,
        },
      };
    }

    await next();
  };
}

export function headers(
  schema: any,
  messages?: any,
  options?: ValidatorConfig
) {
  return common('headers', schema, messages, {
    removeAdditional: false,
    ...(options ?? {}),
  });
}

export function pathParameters(
  schema: any,
  messages?: any,
  options?: ValidatorConfig
) {
  return common('pathParameters', schema, messages, options);
}

export function query(schema: any, messages?: any, options?: ValidatorConfig) {
  return common('query', schema, messages, options);
}

export function body(schema: any, messages?: any, options?: ValidatorConfig) {
  return common('body', schema, messages, options);
}

export function handleErrors() {
  return async (request: lambda.Request, callback, next) => {
    const errors: Error[] = _.flatten(
      Object.values(request.locals.validationErrors ?? {})
    );

    if (errors.length <= 0) {
      return await next();
    }

    callback(null, {
      statusCode: 400,
      body: JSON.stringify({
        errors: errors.map((error) => ({
          message: error.message,
        })),
      }),
    });
  };
}

export { configure, extend, validations } from 'indicative/validator';
