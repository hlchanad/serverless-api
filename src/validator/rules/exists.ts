import { getValue, skippable } from 'indicative-utils';
import * as mongoose from 'mongoose';
import * as ObjectId from 'valid-objectid';

export const exists = {
  async: true,
  compile(args) {
    return args;
  },
  async validate(data, field, args, config): Promise<boolean> {
    const value = getValue(data, field);

    if (skippable(value, field, config)) {
      return true;
    }

    const [collection, searchField = '_id', type = 'objectId'] = args;

    if (!collection) {
      throw new Error('missing collection');
    }

    if (type === 'objectId' && !ObjectId.isValid(value)) {
      return false;
    }

    const count = await mongoose.connection.db
      .collection(collection)
      .countDocuments({
        [searchField]:
          type === 'objectId' ? mongoose.Types.ObjectId(value) : value,
      });

    return count > 0;
  },
};
