import * as mongoose from 'mongoose';

const Schema = new mongoose.Schema({
  title: String,

  eventDetail: {
    date: Date,

    translations: [
      {
        locale: { type: String, enum: ['en', 'zh'], required: true },

        terms: String,
      },
    ],
  },

  translations: [
    {
      locale: { type: String, enum: ['en', 'zh'], required: true },

      content: String,
    },
  ],
});

const Model = mongoose.model('article', Schema, 'articles');

export const Article = Model;
