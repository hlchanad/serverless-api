import { lambda, validator } from '@hlchanad/lambda-helper';

import '@init';

import { Article } from '../models';
import { articlePresenter } from '../presenters';

export const getAll = lambda.extend(async () => {
  const articles = await Article.find();
  return { articles: articlePresenter(articles, 'en').toJSON() };
});

export const getById = lambda.extend(
  {
    middlewares: [
      validator.pathParameters({
        id: 'required|string|exists:articles',
      }),
      validator.handleErrors(),
    ],
  },
  async (request) => {
    const article = await Article.findById(request.pathParameters.id);
    return {
      article: articlePresenter(article, 'en').toJSON(),
    };
  }
);

export const create = lambda.extend(
  {
    middlewares: [
      validator.body({
        title: 'string|required',
        translations: 'array|required|min:1',
        'translations.*.locale': 'string|required|in:en,zh',
        'translations.*.content': 'string|required',

        eventDetail: 'object|required',
        'eventDetail.date': 'date|required',
        'eventDetail.translations': 'array|required|min:1',
        'eventDetail.translations.*.locale': 'string|required|in:en,zh',
        'eventDetail.translations.*.terms': 'string|required',
      }),
      validator.handleErrors(),
    ],
  },
  async (request, callback) => {
    const article = await new Article({
      title: request.body.title,
      translations: request.body.translations.map((translation) => ({
        locale: translation.locale,
        content: translation.content,
      })),
      eventDetail: {
        date: request.body.eventDetail.date,
        translations: request.body.eventDetail.translations.map(
          (translation) => ({
            locale: translation.locale,
            terms: translation.terms,
          })
        ),
      },
    }).save();

    callback(null, {
      statusCode: 201,
      body: JSON.stringify({
        article: articlePresenter(article, 'en').toJSON(),
      }),
    });
  }
);
