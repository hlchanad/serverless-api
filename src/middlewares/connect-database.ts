import { database } from '@database';

export async function connectDatabase(request, callback, next) {
  await database.mongo.connect();
  await next();
}
