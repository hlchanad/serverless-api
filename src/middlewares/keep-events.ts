import { logger } from '@helpers';

export async function keepEvents(request, callback, next) {
  logger.info('--- keep events ---');
  request.context.callbackWaitsForEmptyEventLoop = false;
  await next();
}
