import { config, logger } from '@helpers';
import { lambda, validator } from '@hlchanad/lambda-helper';

import '@init';

export const getInfo = lambda.extend(
  {
    middlewares: [
      validator.query({
        isActive: 'boolean',
        articleId: 'required|string|exists:articles',
      }),
      validator.headers({
        'x-token': 'string|required',
      }),
      validator.handleErrors(),
    ],
  },
  async (request) => {
    logger.info('called getInfo');
    return { app: config('app.name'), locals: request.locals };
  }
);
