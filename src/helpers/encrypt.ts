import * as bcrypt from 'bcryptjs';

import { config } from '@helpers';

export function encryptPassword(password: string): string {
  return bcrypt.hashSync(password, config('encrypt.bcrypt.salt-rounds'));
}

export function verifyPassword(password: string, encrypted: string): boolean {
  return bcrypt.compareSync(password, encrypted);
}
