import { get } from 'lodash';

import * as AppConfig from '../config';

export function config(path: string, defaultValue?: any): any {
  return get(AppConfig, path, defaultValue);
}
