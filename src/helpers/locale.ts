export function defaultLocale(): string {
  return 'en'; // TODO
}

export function getTranslation(
  model: any,
  locale: string,
  translationsField: string = 'translations',
  localeField: string = 'locale'
): any | null {
  const translations = model[translationsField] ?? [];

  // test for expected locale
  let translation = translations.find((t) => t[localeField] === locale);

  // fallback to country code
  if (!translation && locale.indexOf('-') > 0) {
    const country = locale.split('-')[0];
    translation = translations.find((t) => t[localeField] === country);
  }

  // fallback to default locale
  if (!translation) {
    translation = translations.find((t) => t[localeField] === defaultLocale());
  }

  return translation;
}
