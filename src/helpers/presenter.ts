import { cloneDeep, isArray, isPlainObject } from 'lodash';
import { morphism } from 'morphism';

import { getTranslation } from './locale';

interface PresenterOptions {
  excludeVersionKey?: boolean;
  versionKeyField?: string;
  replaceMongoId?: boolean;
  showId?: boolean;
  translationField?: string;
  translationsField?: string;
  localeField?: string;
}

interface Schema {
  [key: string]: Schema | string;
}

export function extend(schema?: any, options?: PresenterOptions) {
  const _options: PresenterOptions = {
    excludeVersionKey: true,
    versionKeyField: '__v',
    replaceMongoId: true,
    showId: true,
    translationField: 'translation',
    translationsField: 'translations',
    localeField: 'locale',
    ...(options ?? {}),
  };

  function toPojo(object: any): any {
    return cloneDeep(
      typeof object.toJSON === 'function' ? object.toJSON() : object
    );
  }

  function toPojos(object: any[]): any[] {
    return object.map((obj) => toPojo(obj));
  }

  function toSchema(object: any | null, basePath: string = ''): Schema {
    return Object.entries(object ?? {})
      .map(([key, value]) => {
        if (isPlainObject(value)) {
          return { [key]: toSchema(value, key) };
        }

        if (isArray(value) && key === _options.translationsField) {
          return Object.entries(value[0])
            .filter(([tKey]) => !['_id', _options.localeField].includes(tKey))
            .map(([tKey]) => ({
              [tKey]: basePath
                ? `${basePath}.${_options.translationField}.${tKey}`
                : `${_options.translationField}.${tKey}`,
            }))
            .reduce((result, pair) => ({ ...result, ...pair }), {});
        }

        return { [key]: basePath ? `${basePath}.${key}` : key };
      })
      .reduce((result, parts) => ({ ...result, ...parts }), {});
  }

  function addTranslation(object: any, locale: string): any {
    const result = cloneDeep(object);

    Object.entries(result)
      .filter(([key, value]) => isPlainObject(value))
      .forEach(([key, value]) => (result[key] = addTranslation(value, locale)));

    if (isArray(object[_options.translationsField])) {
      result[_options.translationField] = getTranslation(
        result,
        locale,
        _options.translationsField,
        _options.localeField
      );
    }

    return result;
  }

  return (object: any, locale: string) => ({
    toJSON() {
      let pojo = isArray(object) ? toPojos(object) : toPojo(object);

      let _schema = schema ? schema : toSchema(isArray(pojo) ? pojo[0] : pojo);

      if (_options.excludeVersionKey) {
        if (_schema[_options.versionKeyField] !== undefined) {
          delete _schema[_options.versionKeyField];
        }
      }

      if (_options.replaceMongoId) {
        if (_schema._id !== undefined) {
          _schema = { id: '_id', ..._schema };
          delete _schema._id;
        }
      }

      if (_options.showId) {
        if (_schema.id === undefined) {
          _schema = { id: '_id', ..._schema };
        }
      }

      pojo = isArray(pojo)
        ? pojo.map((pojo) => addTranslation(pojo, locale))
        : addTranslation(pojo, locale);

      return morphism(_schema, pojo);
    },
  });
}
