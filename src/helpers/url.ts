import { config } from './config';

export function url(type: 'api' | 'upload', path: string = ''): string {
  const baseUrl = config(`app.urls.${type}`);
  return path.startsWith('/') ? baseUrl + path : `${baseUrl}/${path}`;
}
