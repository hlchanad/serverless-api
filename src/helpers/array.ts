export function ensureArray<T>(item: T): T[];
export function ensureArray<T>(array: T[]): T[];
export function ensureArray<T>(arr: T | T[]): T[] {
  if (!arr) return [];
  return Array.isArray(arr) ? arr : [arr];
}
