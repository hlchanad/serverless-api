import { isObjectLike } from 'lodash';

import { config } from './config';
import { logger } from './logger';

type Next = () => Promise<void> | void;

type Middleware = (
  request: Request,
  callback,
  next: Next
) => Promise<void> | void;

type ResponseTransformer = (response: any) => Promise<any> | any;

type Pipeline = {
  push: (...middlewares: Middleware[]) => void;
  execute: (request: Request, callback) => Promise<void>;
};

function Pipeline(...middlewares: Middleware[]): Pipeline {
  const stack: Middleware[] = middlewares;

  const push: Pipeline['push'] = (...middlewares) => {
    stack.push(...middlewares);
  };

  const execute: Pipeline['execute'] = async (request: Request, callback) => {
    let prevIndex = -1;

    const runner = async (index: number): Promise<void> => {
      if (index <= prevIndex) {
        throw new Error('next() called multiple times');
      }

      prevIndex = index;

      const middleware = stack[index];

      if (!middleware) return;

      try {
        await middleware(request, callback, () => runner(index + 1));
      } catch (error) {
        logger.error(error.stack);

        const { statusCode, message } = error;

        callback(null, {
          statusCode,
          body: JSON.stringify({
            errors: [{ message }],
          }),
        });
      }
    };

    await runner(0);
  };

  return { push, execute };
}

export interface Request {
  method: string;
  path: string;
  body: string | any;
  pathParameters: any;
  query: any;
  headers: any;
  locals: any;
  getHeader: (path: string) => string;

  // aws original fields
  event: any;
  context: any;
}

export interface HandlerOptions {
  initialize?: () => void;
  middlewares?: Middleware[];
  responseTransformer?: ResponseTransformer;
}

export type HandlerFn = (request: Request, callback) => Promise<any | void>;

const DEFAULT_OPTIONS: HandlerOptions = {
  initialize: () => {},
  middlewares: [],
  responseTransformer: (response) => response,
};

export function extend(options: HandlerOptions, handler: HandlerFn) {
  options = Object.assign({}, DEFAULT_OPTIONS, options);

  if (typeof options.initialize === 'function') {
    options.initialize();
  }

  return async (event, context, callback) => {
    const request = parseEvent(event, context);

    const middlewares: Middleware[] = [...(options.middlewares ?? [])];
    handler && middlewares.push(toMiddleware(handler));

    await Pipeline(...middlewares).execute(request, callbackWrapper(callback));
  };
}

function toMiddleware(handler: HandlerFn): Middleware {
  return async (request: Request, callback) => {
    const response = await handler(request, callback);

    callback(null, {
      statusCode: 200,
      body:
        response &&
        (isObjectLike(response) ? JSON.stringify(response) : response),
    });
  };
}

function parseEvent(event: any, context: any): Request {
  let body;

  try {
    body = JSON.parse(event.body);
  } catch (e) {
    body = event.body;
  }

  return {
    method: event.httpMethod,
    path: event.path,
    body,
    pathParameters: event.pathParameters,
    query: event.queryStringParameters,
    headers: event.headers,
    locals: {},
    getHeader: (path: string): string =>
      event.headers[path] ||
      Object.entries(event.headers).find(
        ([key]) => key.toLowerCase() === path.toLowerCase()
      )?.[1],

    event: event,
    context: context,
  };
}

function callbackWrapper(callback) {
  return (error, data) => {
    if (!data) {
      data = {};
    }

    data.headers = {
      ...(data.headers ?? {}),
      ...config('app.cors'),
      'x-hello': 'world',
    };

    callback(error, data);
  };
}
