import { lambda } from '@helpers';

let _commonOptions: lambda.HandlerOptions = {};

export const commons = {
  configure(options: lambda.HandlerOptions) {
    if (options) _commonOptions = options;

    // common initialization
    _commonOptions?.initialize && _commonOptions?.initialize();
  },
};

export function extend(
  options: lambda.HandlerOptions,
  handler: lambda.HandlerFn
);
export function extend(handler: lambda.HandlerFn);
export function extend(
  optionsOrHandler: lambda.HandlerOptions | lambda.HandlerFn,
  handler?: lambda.HandlerFn
) {
  let options: lambda.HandlerOptions;

  if (typeof optionsOrHandler === 'function') {
    // @ts-ignore
    handler = optionsOrHandler;
    options = {};
  } else {
    options = optionsOrHandler;
  }

  if (!handler) {
    throw new Error('missing handler');
  }

  const handlerOptions = {
    initialize() {
      // handler specific initialization
      options?.initialize && options?.initialize();
    },
    middlewares: [
      // common middlewares
      ...(_commonOptions?.middlewares ?? []),

      // handler specific middlewares
      ...(options.middlewares ?? []),
    ],
  };

  return lambda.extend(handlerOptions, handler);
}
