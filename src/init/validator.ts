import { validator, rules } from '@validator';

export function initialize() {
  validator.configure({
    removeAdditional: true,
  });

  Object.entries(rules).forEach(([name, rule]) => validator.extend(name, rule));
}
