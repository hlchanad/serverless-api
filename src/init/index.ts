import { lambda } from '@hlchanad/lambda-helper';

import * as middlewares from '@middlewares';

import * as validator from './validator';
import { logger } from '@helpers';

lambda.commons.configure({
  initialize() {
    logger.info('--- common initializations ---');
    validator.initialize();
  },
  middlewares: [
    async (request, response, next) => {
      logger.info('--- common middlewares ---');
      await next();
      logger.info('after next()');
      logger.info({ request, response });
    },
    middlewares.keepEvents,
  ],
});
